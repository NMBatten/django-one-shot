from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from .forms import ListForm, ItemForm


def todo_list_list(request):
    lists = TodoList.objects.all()
    context = {
        "todolist_list": lists
    }
    return render(request, "todos/list.html", context)

def todo_list_detail(request, id):
    current_list = get_object_or_404(TodoList, id=id)
    context = {
        "todo_list": current_list
    }
    return render(request, "todos/detail.html", context)

def todo_list_create(request):
    if request.method == "POST":
        form = ListForm(request.POST)
        if form.is_valid():
            current_list = form.save()
            return redirect("todo_list_detail", id=current_list.id)
    else:
        form = ListForm()

    context = {
        "form": form,
    }

    return render(request, "todos/create.html", context)

def todo_list_update(request, id):
    current_list = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = ListForm(request.POST, instance=current_list)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=id)
    else:
        form = ListForm(instance=current_list)
    context = {
        "list_object": current_list,
        "form": form,
    }
    return render(request, "todos/edit.html", context)

def todo_list_delete(request, id):
    current_list = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        current_list.delete()
        return redirect("todo_list_list")
    return render(request, "todos/delete.html")

def todo_item_create(request):
    if request.method == "POST":
        form = ItemForm(request.POST)
        if form.is_valid():
            item = form.save()
            return redirect("todo_list_detail", id=item.list.id )
    else:
        form = ItemForm()

    context = {
        "form": form,
    }
    return render(request, "todos/create_item.html", context)

def todo_item_update(request, id):
    item = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = ItemForm(request.POST, instance=item)
        if form.is_valid():
            this_item = form.save()
            return redirect("todo_list_detail", id=this_item.list.id)
    else:
        form = ItemForm(instance=item)
    context = {
        "form": form,
        "item_object": item,
    }
    return render(request, "todos/edit.html", context)
